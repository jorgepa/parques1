
package presentacion;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import logica.Sumadora;


public class Modelo implements Runnable{
    
    private Sumadora miSistema;
    private VistaPrincipal ventanaPrincipal;
    private Thread hiloDibujo;
    private boolean activado;
    Random rand = new Random();
        


    
    public void iniciar(){


        activado = true;
        getVentanaPrincipal().setSize(1000, 1000);
        getVentanaPrincipal().setVisible(true);
        hiloDibujo = new Thread(this);
        hiloDibujo.start();
    }
    
    public Sumadora getMiSistema(){
        if(miSistema == null){
            miSistema = new Sumadora();
        }
        return miSistema;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if(ventanaPrincipal == null){
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }
    
        public void mover(String ficha) {
            
            System.out.println("la ficha a mover es: "+ficha);
            
    }
    
     public void lanzar() throws IOException{
         System.out.println("lanzar dados....");         
         int d1 = (int)(Math.random() * 6 + 1);
         int d2 = (int)(Math.random() * 6 + 1);
         System.out.println("el dado 1 es: "+d1);
         System.out.println("el dado 2 es: "+d2);
         Image uno = ImageIO.read(getClass().getResource("1.jpeg"));
         Image dos = ImageIO.read(getClass().getResource("2.jpeg"));
         Image tres = ImageIO.read(getClass().getResource("3.jpeg"));
         Image cuatro = ImageIO.read(getClass().getResource("4.jpeg"));
         Image cinco = ImageIO.read(getClass().getResource("5.jpeg"));
         Image seis = ImageIO.read(getClass().getResource("6.jpeg"));
         
         Graphics dado1 = getVentanaPrincipal().getDado1().getGraphics();
         Graphics dado2 = getVentanaPrincipal().getDado2().getGraphics();
         
        switch (d1) {
            case 1:
                dado1.drawImage(uno,0,0,60,60,null, ventanaPrincipal);
                break;
            case 2:
                dado1.drawImage(dos,0,0,60,60,null, ventanaPrincipal);
                break;
            case 3:
                dado1.drawImage(tres,0,0,60,60,null, ventanaPrincipal);
                break;
            case 4:
                dado1.drawImage(cuatro,0,0,60,60,null, ventanaPrincipal);
                break;
            case 5:
                dado1.drawImage(cinco,0,0,60,60,null, ventanaPrincipal);
                break;
            case 6:
                dado1.drawImage(seis,0,0,60,60,null, ventanaPrincipal);
                break;
            default:
                break;
        }
         
        switch (d2) {
            case 1:
                dado2.drawImage(uno,0,0,60,60,null, ventanaPrincipal);
                break;
            case 2:
                dado2.drawImage(dos,0,0,60,60,null, ventanaPrincipal);
                break;
            case 3:
                dado2.drawImage(tres,0,0,60,60,null, ventanaPrincipal);
                break;
            case 4:
                dado2.drawImage(cuatro,0,0,60,60,null, ventanaPrincipal);
                break;
            case 5:
                dado2.drawImage(cinco,0,0,60,60,null, ventanaPrincipal);
                break;
            case 6:
                dado2.drawImage(seis,0,0,60,60,null, ventanaPrincipal);
                break;
            default:
                break;
        }
 
     }

    public void dibujar() throws IOException{
        
        

        Image back = ImageIO.read(getClass().getResource("tablero.jpg"));
        Image rojo1 = ImageIO.read(getClass().getResource("rojo1.png"));
        Image rojo2 = ImageIO.read(getClass().getResource("rojo2.png"));
        Image verde1 = ImageIO.read(getClass().getResource("verde1.png"));
        Image verde2 = ImageIO.read(getClass().getResource("verde2.png"));
        Image azul1 = ImageIO.read(getClass().getResource("azul1.png"));
        Image azul2 = ImageIO.read(getClass().getResource("azul2.png"));
        Image amarillo1 = ImageIO.read(getClass().getResource("amarillo1.png"));
        Image amarillo2 = ImageIO.read(getClass().getResource("amarillo2.png"));
        Image dadoI = ImageIO.read(getClass().getResource("1.jpeg"));
        
        
        Graphics tablero = getVentanaPrincipal().getLienzo().getGraphics();
        Graphics ficha = getVentanaPrincipal().getRojo1().getGraphics();
        Graphics fichaRojo2 = getVentanaPrincipal().getRojo2().getGraphics();
        Graphics fichaVerde1 = getVentanaPrincipal().getVerde1().getGraphics();
        Graphics fichaVerde2 = getVentanaPrincipal().getVerde2().getGraphics();
        Graphics fichaAzul1 = getVentanaPrincipal().getAzul1().getGraphics();
        Graphics fichaAzul2 = getVentanaPrincipal().getAzul2().getGraphics();
        Graphics fichaAmarillo1 = getVentanaPrincipal().getAmarillo1().getGraphics();
        Graphics fichaAmarillo2 = getVentanaPrincipal().getAmarillo2().getGraphics();
        

        tablero.drawImage(back, 0, 0, 650,650, null, ventanaPrincipal);
        ficha.drawImage(rojo1, 0,0,25,25, ventanaPrincipal);   
        fichaRojo2.drawImage(rojo2, 0, 0, 25,25,null, ventanaPrincipal);
        fichaVerde1.drawImage(verde1, 0, 0, 25,25,null, ventanaPrincipal);
        fichaVerde2.drawImage(verde2, 0, 0, 25,25,null, ventanaPrincipal);
        fichaAzul1.drawImage(azul1, 0, 0, 25,25,null, ventanaPrincipal);
        fichaAzul2.drawImage(azul2, 0, 0, 25,25,null, ventanaPrincipal);
        fichaAmarillo1.drawImage(amarillo1, 0, 0, 25,25,null, ventanaPrincipal);
        fichaAmarillo2.drawImage(amarillo2, 0, 0, 25,25,null, ventanaPrincipal);
    }
    
    
    
    @Override
    public void run() {
        while(activado){
            try {
                dibujar();
            } catch (IOException ex) {
                Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    
}
